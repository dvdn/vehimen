import json


def openfile(filepath, mode='r'):
    with open(filepath, 'r') as f:
        return json.load(f)

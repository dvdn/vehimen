import os
import vehicle

DIR_VEHICLES = "../vehicles"
MSG_CHOOSE = "Choose a vehicle to edit or create a new vehicle file.\n"


def list_vehicles():
    """Get vehicles list"""
    choices_list = []
    for filename in os.listdir(DIR_VEHICLES):
        if filename.endswith(".json"):
            choices_list.append(filename)
    choices_list.append("create a new item")
    return choices_list


def display_list_choices(itemslist):
    """Display vehicles list"""
    print("Your vehicles :")
    for i, name in enumerate(itemslist):
        if ".json" in name:
            name = name[:-5].lower()
        print("  {} - {}".format(i + 1, name))


def valid_int_choice(choice, given_choices):
    """Check input given ('int' in a range of available items)"""
    try:
        choice = int(choice)
    except ValueError:
        print("'{}' n'est pas dans la liste. Entrez un chiffre".format(choice))
        return False

    try:
        choice = int(choice)-1
        if choice not in range(0, len(given_choices)):
            raise IndexError("Choix possible(s) de 1 à {}".format(len(given_choices)))
    except IndexError as error:
        print("Choix invalide. {}".format(error))
        return False

    return True


def display(data_vehicle):
    os.system("cls" if os.name == "nt" else "clear")
    # if create do something else
    path = "{}/{}".format(DIR_VEHICLES, data_vehicle)
    hydrated_vehicle = vehicle.hydrate_vehicle(path)
    for k in hydrated_vehicle.__dict__['_properties'].keys():
        print("{} : {}".format(k, hydrated_vehicle[k]))


if __name__ == "__main__":
    #------List names in order
    #------Choose a vehicle
    #------Read infos
    #JSONschema events and notes
    #Edit Add new infos
    #If no file, Create vehicle
    vehicles = list_vehicles()
    display_list_choices(vehicles)
    choice = input(MSG_CHOOSE)
    while not valid_int_choice(choice, vehicles):
        choice = input(MSG_CHOOSE)
    
    display(vehicles[int(choice)-1])



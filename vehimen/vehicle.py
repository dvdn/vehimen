import utils
import python_jsonschema_objects as pjs


def init_vehicle_schema():
    data = utils.openfile('./vehicle_schema.json')
    builder = pjs.ObjectBuilder(data)
    namespace = builder.build_classes()
    return namespace.VehicleSchema


def hydrate_vehicle(filepath):
    data = utils.openfile(filepath)
    vehicle = init_vehicle_schema()
    hydrated_vehicle = vehicle()
    keys_with_data = set(vehicle.__dict__.keys()).intersection(data.keys())
    # print(vehicle.__dict__['__prop_names__'].keys())
    for k in keys_with_data:
        if k in ['year', 'odometer']:
            data[k] = int(data[k])
        hydrated_vehicle[k] = data[k]
    return hydrated_vehicle


if __name__ == '__main__':
    hydrate_vehicle('../vehicles/vehicle_basic.json')
